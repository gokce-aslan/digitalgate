Pod::Spec.new do |s|

  s.authors      = "Turkcell",
  s.name         = "DigitalGate"
  s.summary      = "Login SDK"
  s.version      = "5.1"
  s.homepage     = "https://www.turkcell.com.tr"
  s.license      = { :type => 'Apache 2.0', :file => 'LICENSE' }

  s.platform     = :ios, "9.0"
  s.source       = { :git => "https://bitbucket.org/gokce-aslan/digitalgate/src"}

  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #

  s.source_files        = "Framework/DigitalGate.framework/Headers/*.h"
  s.public_header_files = "Framework/DigitalGate.framework/Headers/*.h"
  s.vendored_frameworks = "Framework/DigitalGate.framework"
  s.frameworks          = "UIKit", 'Foundation'


end
